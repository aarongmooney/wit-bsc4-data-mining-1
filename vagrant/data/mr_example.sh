#!/usr/bin/env bash

cd ~/hadoop-dist/
bin/hdfs dfs -rm -R /user/vagrant/output
bin/hdfs dfs -mkdir /user
bin/hdfs dfs -mkdir /user/vagrant
bin/hdfs dfs -mkdir input
bin/hdfs dfs -put etc/hadoop/*.xml input
bin/hadoop jar share/hadoop/mapreduce/hadoop-mapreduce-examples-3.1.1.jar grep input output 'dfs[a-z.]+'
bin/hdfs dfs -cat output/*
