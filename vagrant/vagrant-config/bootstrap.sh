#!/usr/bin/env bash

sudo apt-get update

sudo apt-get install -y python3
sudo apt-get install -y python-pip

sudo pip install --upgrade pip

sudo pip install numpy
sudo pip install scikit-learn
sudo pip install seaborn
sudo pip install pandas

sudo apt-get install -y default-jre
sudo apt-get install -y default-jdk

cat /vagrant_data/etc_env >> /etc/environment

sudo -u vagrant ssh-keygen -t rsa -P '' -f /home/vagrant/.ssh/id_rsa
cat /home/vagrant/.ssh/id_rsa.pub >> /home/vagrant/.ssh/authorized_keys
sudo -u vagrant chmod 0600 /home/vagrant/.ssh/authorized_keys
sudo -u vagrant ssh-keyscan -H localhost >> /home/vagrant/.ssh/known_hosts

set -a; source /etc/environment; set +a;

curl -O http://mirrors.whoishostingthis.com/apache/hadoop/common/hadoop-3.1.1/hadoop-3.1.1.tar.gz

tar -xvzf hadoop-3.1.1.tar.gz
mv hadoop-3.1.1 hadoop-dist
cd hadoop-dist/

cp /vagrant_data/*.xml ./etc/hadoop/

bin/hdfs namenode -format
sbin/start-dfs.sh
sbin/start-yarn.sh

cp /vagrant_data/mr_example.sh /home/vagrant/
chmod +x /home/vagrant/mr_example.sh

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password secret'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password secret'
sudo apt-get -y install mysql-server
